const navSlide = () => {
  const navBurgerBtn = document.getElementById("nav-burger");
  const nav = document.querySelector(".nav-links");

  const navLinksAll = document.querySelectorAll(".nav-links li");
  navBurgerBtn.addEventListener("click", () => {
    //toggle nav
    nav.classList.toggle("nav-active");

    //animate links
    navLinksAll.forEach((link, index) => {
      if (link.style.animation) {
        link.style.animation = "";
      } else {
        link.style.animation = `navLinkFade 0.5s ease forwards ${
          index / 7 + 0.3
        }s`;
      }
    });
    //burger animation
    navBurgerBtn.classList.toggle("toggle");
  });
};

navSlide();
